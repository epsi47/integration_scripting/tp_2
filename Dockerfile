FROM python:alpine3.16

WORKDIR /app

COPY project.py project.py

CMD python3 project.py